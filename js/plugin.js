(function () {
    let mapLoading = false
    let mapLoaded = false

    let loadedCallbacks = []

    const loadApiScripts = (apiKey, selector, config) => {
        if (mapLoading || mapLoaded) {
            return
        }

        if ( ! document.body.classList.contains('wp-admin') && 'undefined' !== typeof window.cookieConsent) {
            window.cookieConsent.addGeneralCallback(() => {
                loadApiScripts(apiKey, selector, config)
            })

            if ( ! window.cookieConsent.isAllowed()) {
                document.querySelector(selector).parentNode.parentNode.classList.add('no-consent')

                document.querySelector(selector).parentNode.parentNode.querySelector('#reopen-maps-consent').addEventListener('click', () => {
                    window.cookieConsent.showLayer()
                })

                return
            }
        }

        if ( ! document.body.classList.contains('wp-admin') && config.cookieConsent.enabled) {
            const checkFunction = window[config.cookieConsent.checkCallback]
            const reopenFunction = window[config.cookieConsent.reopenCallback]

            if ('undefined' === typeof checkFunction) {
                throw new Error('Function does not exist: ' + config.cookieConsent.checkCallback)
            }

            if ('undefined' === typeof reopenFunction) {
                throw new Error('Function does not exist: ' + config.cookieConsent.reopenCallback)
            }

            if ( ! checkFunction()) {
                document.querySelector(selector).parentNode.parentNode.classList.add('no-consent')

                document.querySelector(selector).parentNode.parentNode.querySelector('#reopen-maps-consent').addEventListener('click', () => {
                    const callback = () => {
                        loadApiScripts(apiKey, selector, config)
                    }

                    reopenFunction(callback)
                })

                return
            }
        }

        document.querySelector(selector).parentNode.parentNode.classList.remove('no-consent')

        mapLoading = true

        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${apiKey}&callback=initMap`
        script.async = true;

        window.initMap = () => {
            mapLoaded = true
            loadedCallbacks.forEach(f => f())
        }

        document.head.appendChild(script);
    }

    window.loadSFMap = (apiKey, selector, config, generalConfig = {}) => {
        if ( ! mapLoaded) {
            if ( ! mapLoading) {
                loadApiScripts(apiKey, selector, generalConfig)
            }

            loadedCallbacks.push(() => loadSFMap(apiKey, selector, config, generalConfig))
            return
        }

        const gmapConfig = {
            center: { lat: config.center.lat, lng: config.center.lng },
            zoom: config.zoom,
        }

        const icon = {
            url: generalConfig.marker?.url || config.marker
        }

        if ('undefined' !== typeof generalConfig.marker?.scaledSize) {
            icon.scaledSize = new google.maps.Size(generalConfig.marker.scaledSize[0], generalConfig.marker.scaledSize[1])
        }
        if ('undefined' !== typeof generalConfig.marker?.origin) {
            icon.origin = new google.maps.Point(generalConfig.marker.origin[0], generalConfig.marker.origin[1])
        }
        if ('undefined' !== typeof generalConfig.marker?.anchor) {
            icon.anchor = new google.maps.Point(generalConfig.marker.anchor[0], generalConfig.marker.anchor[1])
        }

        const map = new google.maps.Map(document.querySelector(selector), gmapConfig);

        if ('undefined' !== typeof config.polygon) {
            const paths = parseSFMapPolygonSettings(config.polygon)

            const polygon = new google.maps.Polygon({
                ...generalConfig.polygonConfig,
                ...{ paths }
            });

            polygon.setMap(map);
        }

        if ('undefined' !== typeof config.markers) {
            config.markers.forEach(m => {
                const marker = new google.maps.Marker({
                    position: {
                        lat: m.position.lat,
                        lng: m.position.lng
                    },
                    icon,
                    animation: google.maps.Animation.DROP,
                    title: m.name
                })

                marker.setMap(map)

                const infowindow = new google.maps.InfoWindow({
                    content: `<h3 class="small">${m.name}</h3>`
                });

                marker.addListener('click', () => infowindow.open(map, marker))
            })
        }
    }

    window.parseSFMapPolygonSettings = settings => {
        const lines = settings.split("\n")

        const coordinates = []

        for (const line of lines) {
            const matches = line.trim().match(/^([\d\.]+), ([\d\.]+)$/)
            if (matches) {
                const [_, lng, lat] = matches
                coordinates.push({lng: parseFloat(lng), lat: parseFloat(lat)})
            }
        }

        return coordinates
    }
})()

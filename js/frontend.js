function loadMaps(apiKey, jsonMaps, jsonGeneralConfig = null) {
    const configContainer = document.querySelector('#json-maps-config')
    const config = configContainer.innerHTML ? JSON.parse(configContainer.innerHTML.trim()) : {}

    const generalConfig = jsonGeneralConfig ? JSON.parse(jsonGeneralConfig) : {}

    document.querySelectorAll('.wp-block-sf-maps-block .map').forEach(map => {
        const id = map.id
        const mapId = map.getAttribute('data-map-id')

        const configSearchResult = 'undefined' !== typeof config.maps ? config.maps.filter(m => m.id == mapId) : []

        if (configSearchResult.length) {
            const mapConfig = configSearchResult[0]

            loadSFMap(apiKey, `#${id}`, mapConfig, generalConfig)
        }
    })
}
<?php
/**
 * Plugin Name:       SF Maps
 * Description:       Google-Maps Konfigurator
 * Version:           1.0.0
 * Author:            Mario Klug <mario.klug@sourcefactory.at>
 */

if ( ! defined( 'WPINC' ) ) {
    die;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'block' . DIRECTORY_SEPARATOR . 'init.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR . 'init.php';

class SFMapMarker implements JsonSerializable {
    public $url;
    public $scaledSize = [47, 70];
    public $origin = [];
    public $anchor = [];

    public function __construct()
    {
        $this->url = get_template_directory_uri() . '/assets/maps-marker.png';
    }

    public function jsonSerialize()
    {
        $res = [
            'url' => $this->url
        ];

        if ($this->scaledSize) {
            $res['scaledSize'] = $this->scaledSize;
        }

        if ($this->origin) {
            $res['origin'] = $this->origin;
        }

        if ($this->anchor) {
            $res['anchor'] = $this->anchor;
        }

        return $res;
    }
}

class SFMapsPolygonConfig {
    public $strokeColor = '#FF0000';
    public $strokeOpacity = 0.8;
    public $strokeWeight = 2;
    public $fillColor = '#FF0000';
    public $fillOpacity = 0.35;
}

class SFMapCookieConsent implements JsonSerializable
{
    private $enabled = false;
    private $checkCallback = null;
    private $reopenCallback = null;

    public function enable(string $checkCallback, string $reopenCallback)
    {
        $this->enabled = true;
        $this->checkCallback = $checkCallback;
        $this->reopenCallback = $reopenCallback;
    }

    public function jsonSerialize()
    {
        if ( ! $this->enabled) {
            return ['enabled' => false];
        }

        return [
            'enabled' => true,
            'checkCallback' => $this->checkCallback,
            'reopenCallback' => $this->reopenCallback
        ];
    }
}

class SFMapsConfig {
    public $marker = null;
    public $polygonConfig = null;
    public $cookieConsent = null;

    public function __construct()
    {
        $this->marker = new SFMapMarker();
        $this->polygonConfig = new SFMapsPolygonConfig();
        $this->cookieConsent = new SFMapCookieConsent();
    }
}

add_action('enqueue_block_assets', function() {
    wp_enqueue_script('plugin-outtake-map', plugin_dir_url(__FILE__) . 'js/plugin.js', [], '1.0.2');
});

add_action('init', function() {
    wp_enqueue_script('plugin-outtake-map', plugin_dir_url(__FILE__) . 'js/plugin.js', [], '1.0.2');

    if ( ! is_admin()) {
        wp_enqueue_script('plugin-outtake-map-frontend', plugin_dir_url(__FILE__) . 'js/frontend.js', [], '1.0.2');
    }
});

add_action('get_footer', function() {
    $jsonMaps = get_option('sf_maps_config');
    $generalConfig = apply_filters('sf_maps_config', new SFMapsConfig());
?>
    <textarea id="json-maps-config" style="display: none"><?= $jsonMaps ?></textarea>
<script>
window.addEventListener('load', () => {
    loadMaps('<?= get_option('sf_maps_api_key') ?>', '<?= $jsonMaps ?>', '<?= json_encode($generalConfig) ?>')
})
</script>
<?php
});

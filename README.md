## Available filters

Customization is possible by adding filters to themes functions.php.

```php
<?php
add_filter('sf_maps_block_name', fn() => 'Karten');

add_filter('sf_maps_block_category', fn() => 'weiss');

add_filter('sf_maps_config', function(SFMapsConfig $config) 
{
    $config->marker->origin = [0, 0];
    $config->marker->anchor = [23, 70];
    
    $config->polygonConfig->strokeColor = '#f56020';
    $config->polygonConfig->fillColor = '#f56020';
    
    $config->cookieConsent->enable('areCookiesAllowed', 'openCookieConsentLayer');
    
    return $config;
});
```

### Configuration

To enable respect to cookie consent 2 javascript function names must be provided
which have to be accessible on the window object.

`areCookiesAllowed(): bool` - returns true of false whether google-scripts are allowed.

`openCookieConsentLayer(callback: (void) => void): void` - should start consent layer and
inform this plugin about changes using the callback function.

If the plugin `sf-cookie-consent` is installed it's automatically detected and will be 
used.
import { registerBlockType } from '@wordpress/blocks';
import { useBlockProps, InspectorControls } from '@wordpress/block-editor';
import { useEntityProp } from '@wordpress/core-data';
import { SelectControl, TextControl, PanelBody } from '@wordpress/components'
import { __ } from '@wordpress/i18n'

const initializedMaps = {}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

let generalSettings = {}

registerBlockType( 'sf/maps-block', {
    apiVersion: 2,
    icon: 'location-alt',
    example: {},

    attributes: {
        clientId: {
            type: 'string'
        },
        mapId: {
            type: 'number',
            default: 0
        },
        height: {
            type: 'number',
            default: 500
        }
    },

    edit({ clientId, attributes, setAttributes }) {
        const blockProps = useBlockProps();

        const [ apiKey ] = useEntityProp( 'root', 'site', 'sf_maps_api_key' );
        const [ mapConfig ] = useEntityProp( 'root', 'site', 'sf_maps_config' );
        let [ settings ] = useEntityProp( 'root', 'site', 'sf_maps_settings' );

        if ( ! attributes.clientId) {
            setAttributes({ clientId })
        }

        if (settings) {
            generalSettings = JSON.parse(settings)
        }

        const availableMaps = [{label: '-', value: 0}]

        if (mapConfig) {
            const config = JSON.parse(mapConfig)

            config.maps.forEach(c => {
                availableMaps.push({
                    value: c.id,
                    label: c.name
                })
            })

            if (apiKey
                && attributes.mapId > 0
                && ('undefined' === typeof initializedMaps[clientId] || parseInt(initializedMaps[clientId]) !== parseInt(attributes.mapId))
            ) {
                const configForSingleMap = config.maps.filter(m => parseInt(m.id) === parseInt(attributes.mapId))[0]

                setTimeout(() => {
                    loadSFMap(apiKey, `#outtake-map-${clientId}`, configForSingleMap, generalSettings)
                }, 100)

                initializedMaps[clientId] = attributes.mapId
            }
        }

        const containerClasses = ['wrapper']
        const containerStyle = {}

        if (attributes.mapId < 1) {
            containerClasses.push('no-map')

            if ('undefined' !== typeof initializedMaps) {
                delete initializedMaps[clientId]
            }
        } else {
            containerStyle.height = `${attributes.height}px`
        }

        return (
            <div { ...blockProps}>
                <InspectorControls>
                    <PanelBody>
                        <SelectControl
                            label="Karte"
                            value={attributes.mapId}
                            options={availableMaps}
                            onChange={mapId => setAttributes({mapId: parseInt(mapId)})}
                        />
                        {attributes.mapId > 0 && <TextControl
                            type="range"
                            label="Höhe"
                            min={300}
                            max={800}
                            value={attributes.height}
                            onChange={value => setAttributes({height: parseInt(value)})}/>}
                    </PanelBody>
                </InspectorControls>
                <div className={containerClasses.join(' ')} style={containerStyle}>
                    {attributes.mapId > 0
                        && <div className="map" id={`outtake-map-${clientId}`}/>
                        || <span>Bitte wählen Sie rechts die gewünschte Karte aus</span>}
                </div>
            </div>
        );
    },

    save: ({ attributes }) => {
        const noConsentMessage = __('Sie haben dem Nachladen externer Dateien nicht zugestimmt wodurch diese Karte nicht angezeigt werden kann.')
        const openSettingsMessage = __('Einstellungen erneut öffnen')

        return (
            <div className="wp-block-sf-maps-block">
                <div className="wrapper" style={{ height: `${attributes.height}px`}}>
                    <div className="map" id={`outtake-map-${attributes.clientId}`} data-map-id={attributes.mapId}/>
                    <span className="no-consent">
                        <p>
                            { noConsentMessage }
                        </p>
                        <a href="javascript:void(0)" id="reopen-maps-consent" className="button sf-button sf-button-black">
                            { openSettingsMessage }
                        </a>
                    </span>
                </div>
            </div>
        );
    }
} );

<?php
add_action( 'init', function() {
    wp_register_script('sf-maps-block', plugin_dir_url(__FILE__) . '/build/index.js', [ 'wp-blocks', 'wp-element', 'wp-block-editor', 'wp-core-data', 'wp-i18n' ]);
    wp_enqueue_style('sf-maps-block', plugin_dir_url(__FILE__) . '/build/index.css');

    $generalConfig = apply_filters('sf_maps_config', new SFMapsConfig());

    update_option('sf_maps_settings', json_encode($generalConfig));

    register_setting('sf-maps', 'sf_maps_settings', [
        'type' => 'string',
        'show_in_rest' => true
    ]);

    register_block_type('sf/maps-block', [
        'title' => apply_filters('sf_maps_block_name', 'Karten'),
        'category' => apply_filters('sf_maps_block_category', 'sourcefactory'),
        'api_version' => 2,
        'editor_script' => 'sf-maps-block',
        'editor_style' => 'sf-maps-block',
        'attributes' => [
            'mapId' => [
                'type' => 'number'
            ],
            'height' => [
                'type' => 'number'
            ]
        ]
    ]);
});

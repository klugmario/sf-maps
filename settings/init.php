<?php
add_action( 'admin_menu', function() {
    add_menu_page('Karten', 'Karten', 'manage_options', 'sf-maps', 'sf_maps_settings', 'dashicons-location-alt');
//    add_submenu_page('edit.php?post_type=leistungen', 'Einstellungen', 'Einstellungen', 'manage_options', 'services-settings', 'sf_services_settings');
});

function register_maps_settings() {
    register_setting('sf-maps', 'sf_maps_api_key', [
        'type' => 'string',
        'show_in_rest' => true,
        'sanitize_callback' => 'sanitize_text_field',
    ]);

    register_setting('sf-maps', 'sf_maps_config', [
        'type' => 'string',
        'show_in_rest' => true
    ]);
}

add_action( 'admin_init', 'register_maps_settings');
add_action( 'rest_api_init', 'register_maps_settings');

add_action('admin_init', function() {
    if ( ! session_id()) {
        session_start();
    }

    if ('options.php' === basename($_SERVER['SCRIPT_FILENAME'])
        && (isset($_POST['sf_maps_api_key']) || isset($_POST['sf_maps_config']))) {
        $_SESSION['PLUGIN_SF_MAPS_SETTINGS_SAVED'] = true;
    }

    if (sf_maps_is_settings_page()) {
        wp_enqueue_script('plugin-settings', plugin_dir_url(__FILE__) . '/js/settings-page.js');
    }
});

function sf_maps_is_settings_page() {
    return 'admin.php' === basename($_SERVER['SCRIPT_FILENAME'])
        && isset($_GET['page']) && 'sf-maps' === $_GET['page'];
}

add_action( 'admin_notices', function () {
    $apiKey = get_option('sf_maps_api_key');

    if (sf_maps_is_settings_page()) {
        ?>
        <div class="notice notice-info">
            <p>
                Hier können Sie eine beliebige Anzahl an Karten erstellen welche im Seiten-Editor als Blöcke hinzugefügt werden können.
            </p>
        </div>
        <?php
        if (isset($_SESSION['PLUGIN_SF_MAPS_SETTINGS_SAVED'])) {
            unset($_SESSION['PLUGIN_SF_MAPS_SETTINGS_SAVED']);
            ?>
            <div class="notice notice-info is-dismissible">
                <p>
                    Änderungen gespeichert!
                </p>
            </div>
            <?php
        }
        return;
    }

    if ( ! $apiKey) {
    ?>
    <div class="notice notice-error">
        <p>
            Es wurde noch kein Google-Maps API-Schlüssel angegeben!<br>
            Bitte gehen Sie zu den <a href="admin.php?page=sf-maps">Plugin-Einstellungen</a>.
        </p>
    </div>
    <?php
    }
});

function sf_maps_settings() {
    require __DIR__ . DIRECTORY_SEPARATOR . 'settings-page.php';
}

class MapsConfig
{
    constructor(apiKey) {
        this.apiKey = apiKey

        this.apiLoading = false
        this.apiLoaded = false

        this.map = null

        this.configContainer = document.querySelector('#json-maps-config')

        if ( ! this.configContainer) {
            throw Error('config container #json-maps-config not found')
        }

        this.config = this.configContainer.innerHTML ? JSON.parse(this.configContainer.innerHTML.trim()) : {}

        this.mapConfigIndex = -1
        this.mapConfig = null

        this.locationInfoDiv = null
        this.updateLocationButton = null
        this.resetLocationButton = null

        this.saveCallback = null

        this.apiLoadingQueue = []
    }

    showMap(mapId, selector, config = {}, generalConfig = {}) {
        const { height, showLocator, image, imageSize } = Object.assign({
            height: 400,
            showLocator: false,
            image: null,
            imageSize: [40, 50]
        }, config)

        this.loadMapConfig(mapId)

        const container = document.querySelector(selector)

        if (showLocator) {
            const centerSearchInput = document.createElement('input')
            centerSearchInput.type = 'text'
            centerSearchInput.placeholder = 'Suche'
            centerSearchInput.classList.add('regular-text')

            const p = document.createElement('p')

            p.appendChild(centerSearchInput)
            container.appendChild(p)

            this.initAutoComplete(centerSearchInput, position => this.map.setCenter(position))
        }

        const mapDiv = document.createElement('div')
        mapDiv.style.height = `${height}px`
        container.appendChild(mapDiv)

        const buttonContainer = document.createElement('div')
        buttonContainer.style.marginTop = '10px'

        if (showLocator) {
            this.locationInfoDiv = document.createElement('div')
            this.locationInfoDiv.classList.add('location-info')

            this.updateLocationButton = document.createElement('a')
            this.updateLocationButton.classList.add('button')
            this.updateLocationButton.classList.add('button-primary')
            this.updateLocationButton.classList.add('disabled')
            this.updateLocationButton.innerHTML = 'Änderungen übernehmen'
            this.updateLocationButton.addEventListener('click', () => this.saveLocation())

            this.resetLocationButton = document.createElement('a')
            this.resetLocationButton.classList.add('button')
            this.resetLocationButton.classList.add('button-primary')
            this.resetLocationButton.classList.add('disabled')
            this.resetLocationButton.innerHTML = 'Änderungen verwerfen'
            this.resetLocationButton.style.marginLeft = '10px'
            this.resetLocationButton.style.marginRight = '10px'
            this.resetLocationButton.addEventListener('click', () => this.resetLocation())

            buttonContainer.appendChild(this.updateLocationButton)
            buttonContainer.appendChild(this.resetLocationButton)

            container.appendChild(this.locationInfoDiv)
        }

        const removeMapButton = document.createElement('a')
        removeMapButton.classList.add('button')
        removeMapButton.classList.add('button-error')
        removeMapButton.innerHTML = 'Karte löschen (!)'
        removeMapButton.addEventListener('click', () => this.removeMap())

        buttonContainer.appendChild(removeMapButton)
        container.appendChild(buttonContainer)

        if ( ! container) {
            throw Error(`map container ${selector} not found`)
        }

        if ( ! this.apiLoaded) {
            this.loadApiScripts(() => {
                const wrapper = document.createElement('div')
                wrapper.style.height = 'calc(100% - 15px)'
                wrapper.style.border = '5px solid #fff'

                mapDiv.appendChild(wrapper)

                this.map = new google.maps.Map(wrapper, {
                    center: { lat: this.mapConfig.center.lat, lng: this.mapConfig.center.lng },
                    zoom: this.mapConfig.zoom,
                })

                google.maps.event.addListener(this.map, "center_changed", () => {
                    this.updateZoomLocation()
                })

                google.maps.event.addListener(this.map, "zoom_changed", () => {
                    this.updateZoomLocation()
                })

                if ('undefined' !== typeof this.mapConfig.polygon) {
                    const polygonCoordinates = window.parseSFMapPolygonSettings(this.mapConfig.polygon)

                    const polygon = new google.maps.Polygon({
                        paths: polygonCoordinates,
                        strokeColor: "#FF0000",
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: "#FF0000",
                        fillOpacity: 0.35,
                    });

                    polygon.setMap(this.map);
                }

                const icon = {
                    url: generalConfig.marker?.url || config.marker
                }

                if ('undefined' !== typeof generalConfig.marker?.scaledSize) {
                    icon.scaledSize = new google.maps.Size(generalConfig.marker.scaledSize[0], generalConfig.marker.scaledSize[1])
                }
                if ('undefined' !== typeof generalConfig.marker?.origin) {
                    icon.origin = new google.maps.Point(generalConfig.marker.origin[0], generalConfig.marker.origin[1])
                }
                if ('undefined' !== typeof generalConfig.marker?.anchor) {
                    icon.anchor = new google.maps.Point(generalConfig.marker.anchor[0], generalConfig.marker.anchor[1])
                }

                if ('undefined' !== typeof this.mapConfig.markers) {
                    this.mapConfig.markers.forEach(m => {
                        const marker = new google.maps.Marker({
                            position: {
                                lat: m.position.lat,
                                lng: m.position.lng
                            },
                            icon,
                            animation: google.maps.Animation.DROP,
                            title: m.name
                        })

                        marker.setMap(this.map)
                    })
                }

                this.updateZoomLocation()
            })
        }

        return this
    }

    showMarkersTable(mapId, selector) {
        const container = document.querySelector(selector)

        this.loadMapConfig(mapId)

        if ('undefined' === typeof this.mapConfig.markers || ! this.mapConfig.markers) {
            return this
        }

        const rows = this.mapConfig.markers.map(m => {
            const link = '<a href="javascript:void(0);" class="sf-map-marker-remove" data-id="' + m.id + '">[löschen]</a>'

            const addressField = m.address || null

            return [ m.id, m.name, addressField, m.position.lat + ' / ' + m.position.lng, link ]
        })

        const table = this.createTable(['#', 'Bezeichnung', 'Adresse', 'Koordinaten', null], rows)

        container.appendChild(table)

        document.querySelectorAll('.sf-map-marker-remove').forEach(link => {
            link.addEventListener('click', e => {
                if ( ! confirm('Diese Markierung wirklich löschen?')) {
                    return
                }

                const markerId = e.target.getAttribute('data-id')

                this.mapConfig.markers = this.mapConfig.markers.filter(m => parseInt(m.id) !== parseInt(markerId))

                this.updateConfig()
            })
        })

        return this
    }

    showPolygonSettings(mapId, selector) {
        const textarea = document.querySelector(selector)
        const btn = document.querySelector('#btn-polygon')

        this.loadMapConfig(mapId)

        this.initialPolygonSettings = textarea.value

        if ('undefined' !== typeof this.mapConfig.polygon && this.mapConfig.polygon) {
            this.initialPolygonSettings = this.mapConfig.polygon
            textarea.value = this.mapConfig.polygon
        }

        const onTextChange = () => {
            const settings = textarea.value

            if (settings !== this.initialPolygonSettings) {
                btn.classList.remove('disabled')
            } else {
                btn.classList.add('disabled')
            }
        }

        textarea.addEventListener('keyup', onTextChange)
        textarea.addEventListener('change', onTextChange)

        btn.addEventListener('click', () => {
            if (btn.classList.contains('disabled')) {
                return
            }

            this.mapConfig.polygon = textarea.value

            this.updateConfig()
        })

        return this
    }

    onSave(callback) {
        this.saveCallback = callback

        return this
    }

    loadMapConfig(mapId) {
        for (let i in this.config.maps) {
            if (parseInt(this.config.maps[i].id) === parseInt(mapId)) {
                this.mapConfigIndex = i
                this.mapConfig = this.config.maps[i]
                return
            }
        }

        throw Error(`map with id ${mapId} not found in configuration`)
    }

    updateZoomLocation() {
        const config = this.getZoomLocation()

        if (this.locationInfoDiv) {
            this.locationInfoDiv.innerText = `Zoomlevel: ${config.zoom}, Koordinaten: ${config.location.lat} / ${config.location.lng}`
        }

        if (this.updateLocationButton) {
            const currentSettings = '' + [this.mapConfig.center.lat, this.mapConfig.center.lng, this.mapConfig.zoom].join('|')
            const newSettings = '' + [config.location.lat, config.location.lng, config.zoom].join('|')

            if (currentSettings !== newSettings) {
                this.updateLocationButton.classList.remove('disabled')
                this.resetLocationButton.classList.remove('disabled')
            } else {
                this.updateLocationButton.classList.add('disabled')
                this.resetLocationButton.classList.add('disabled')
            }
        }
    }

    getZoomLocation() {
        const zoom = this.map.getZoom()
        const center = this.map.getCenter()

        const location = {
            lat: Math.round(center.lat() * 1000, 5) / 1000,
            lng: Math.round(center.lng() * 1000, 5) / 1000
        }

        return { zoom, location }
    }

    saveLocation() {
        const config = this.getZoomLocation()

        this.mapConfig.zoom = config.zoom
        this.mapConfig.center.lat = config.location.lat
        this.mapConfig.center.lng = config.location.lng

        this.updateConfig()
    }

    resetLocation() {
        this.map.setZoom(this.mapConfig.zoom)
        this.map.setCenter({ lat: this.mapConfig.center.lat, lng: this.mapConfig.center.lng })
    }

    updateConfig() {
        if (this.mapConfigIndex) {
            this.config.maps[this.mapConfigIndex] = this.mapConfig
            this.configContainer.innerHTML = JSON.stringify(this.config)

            if (this.saveCallback) {
                this.saveCallback()
            }
        }
    }

    loadApiScripts(callback) {
        if (this.apiLoading) {
            return this.apiLoadingQueue.push(callback)
        }

        if (this.apiLoaded) {
            return callback()
        }

        this.apiLoading = true

        const script = document.createElement('script');
        script.src = `https://maps.googleapis.com/maps/api/js?key=${this.apiKey}&libraries=places&callback=initMap`
        script.async = true;

        window.initMap = () => {
            this.apiLoading = false
            this.apiLoaded = true

            callback()

            this.apiLoadingQueue.forEach(cb => cb())
        }

        document.head.appendChild(script);
    }

    createMap(mapName, marker, imageSize) {
        if ('undefined' === typeof this.config.maps) {
            this.config.maps = []
        }

        if (this.config.maps.map(map => map.name.toLowerCase()).includes(mapName.toLowerCase())) {
            throw Error('Eine Karte mit dieser Bezeichnung existiert bereits')
        }

        const maxId = this.config.maps.length ? Math.max(...this.config.maps.map(map => map.id)) : 0

        this.config.maps.push({
            id: maxId + 1,
            name: mapName,
            zoom: 14,
            marker,
            imageSize,
            center: {
                lat: 46.966,
                lng: 15.503
            }
        })

        this.configContainer.innerHTML = JSON.stringify(this.config)

        if (this.saveCallback) {
            this.saveCallback()
        }

        return this
    }

    removeMap() {
        if (confirm('Möchten Sie diese Karte wirklich unwiderruflich löschen?')) {
            this.config.maps = this.config.maps.filter(map => parseInt(map.id) !== parseInt(this.mapConfig.id))

            this.configContainer.innerHTML = JSON.stringify(this.config)

            if (this.saveCallback) {
                this.saveCallback()
            }
        }
    }

    initAutoComplete(input, callback) {
        this.loadApiScripts(() => {
            const searchBox = new google.maps.places.SearchBox(input);

            searchBox.addListener('places_changed', () => {
                const places = searchBox.getPlaces()

                if (places.length) {
                    const location = places[0].geometry.location

                    callback({ lat: location.lat(), lng: location.lng() })
                }
            })
        })
    }

    addMarker(name, address, position) {
        if ('undefined' === typeof this.mapConfig.markers) {
            this.mapConfig.markers = []
        }

        let maxId = this.mapConfig.markers.length ? Math.max(...this.mapConfig.markers.map(m => m.id)) : 0

        this.mapConfig.markers.push({
            id: maxId + 1,
            name,
            address,
            position
        })

        this.updateConfig()
    }

    createTable(header, rows) {
        const table = document.createElement('table')
        table.classList.add('widefat')

        const thead = document.createElement('thead')
        const tbody = document.createElement('tbody')

        thead.appendChild(this.createTableRow('th', header))

        for (const data of rows) {
            tbody.appendChild(this.createTableRow('td', data))
        }

        table.appendChild(thead)
        table.appendChild(tbody)

        return table
    }

    createTableRow(cellType, arrData) {
        const row = document.createElement('tr')

        for (const value of arrData) {
            const cell = document.createElement(cellType)
            cell.innerHTML = value
            row.appendChild(cell)
        }

        return row;
    }
}

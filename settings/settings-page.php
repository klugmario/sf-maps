<?php
$activeMap = isset($_GET['map']) ? $_GET['map'] : null;

$apiKey = get_option('sf_maps_api_key');

$jsonMaps = get_option('sf_maps_config');
$mapsConfig = $jsonMaps ? json_decode($jsonMaps, true, 512, JSON_OBJECT_AS_ARRAY) : [];

$generalConfig = apply_filters('sf_maps_config', new SFMapsConfig());

if ($activeMap) {
    $mapExists = false;
    if (isset($mapsConfig['maps'])) {
        foreach ($mapsConfig['maps'] as $map) {
            if ($map['id'] == $activeMap) {
                $mapExists = true;
                $activeMapConfig = $map;
                break;
            }
        }
    }

    if ( ! $mapExists) {
        echo '<meta http-equiv="refresh" content="0; URL=admin.php?page=' . $_GET['page'] . '">';
        exit(0);
    }
}
?>

<div class="wrap">
    <h1><?= __('Karten') ?></h1>

    <h2 class="nav-tab-wrapper">
        <a href="?page=<?php echo $_GET['page']; ?>" class="nav-tab <?php echo ! $activeMap ? 'nav-tab-active' : ''; ?>">Allgemein</a>
        <?php if (isset($mapsConfig['maps'])): ?>
            <?php foreach ($mapsConfig['maps'] as $map): ?>
                <a href="?page=<?php echo $_GET['page']; ?>&map=<?= $map['id'] ?>" class="nav-tab <?php echo $activeMap == $map['id'] ? 'nav-tab-active' : ''; ?>">
                    <?= $map['name'] ?>
                </a>
            <?php endforeach ?>
        <?php endif ?>
    </h2>

    <?php if ( ! $activeMap): ?>
        <form method="post" action="options.php" id="settingsform">
            <?php settings_fields( 'sf-maps' ); ?>
            <?php do_settings_sections( 'sf-maps' ); ?>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row">
                        <label for="api-key"><?= __('Google Maps API-Schlüssel') ?></label>
                    </th>
                    <td>
                        <input name="sf_maps_api_key" id="api-key" type="text" class="regular-text" value="<?= $apiKey ?>"/>
                    </td>
                </tr>
            </table>

            <textarea id="json-maps-config" style="display: none" name="sf_maps_config"><?= $jsonMaps ?></textarea>

            <?php submit_button(); ?>
        </form>

        <hr>

        <h2>Neue Karte anlegen</h2>

        <input name="sf_maps_new_map_name" type="text" class="regular-text" id="new-map-name" placeholder="Bezeichnung"/>

        <a href="javascript:void(0);" class="button button-secondary" id="btn-new-map">
            Anlegen
        </a>

        <script>
            const runCreate = () => {
                const mapName = document.getElementById('new-map-name').value.trim()

                if (mapName.length < 1) {
                    alert('Bitten geben Sie eine Bezeichnung für die neue Karte an.')
                }

                const mapsConfig = new MapsConfig('<?= $apiKey ?>')

                try {
                    mapsConfig
                        .onSave(() => {
                            console.log(document.getElementById('settingsform'))
                            document.getElementById('submit').click()
                        })
                        .createMap(mapName, '<?= bloginfo("template_url") ?>/assets/maps-marker.png', [40, 50])
                } catch (e) {
                    alert(e.message)
                }
            }

            document.getElementById('btn-new-map').addEventListener('click', runCreate)
            document.getElementById('new-map-name').addEventListener('keyup', e => {
                if ('Enter' === e.key) {
                    runCreate()
                }
            });
        </script>
    <?php else: ?>
        <form method="post" action="options.php" id="mapform">
            <?php settings_fields( 'sf-maps' ); ?>
            <?php do_settings_sections( 'sf-maps' ); ?>

            <input name="sf_maps_api_key" type="hidden" value="<?= $apiKey ?>"/>

            <div id="map"></div>

            <textarea id="json-maps-config" style="display: none" name="sf_maps_config"><?= $jsonMaps ?></textarea>

            <hr style="margin: 20px 0">

            <h2>Neue Markierung hinzufügen</h2>

            <div>
                <input type="text" class="regular-text" id="new-marker-name" placeholder="Bezeichnung">
                <input type="text" class="regular-text" id="new-marker-position" placeholder="Adresse">
                <a class="button button-primary disabled" id="btn-add-marker">Hinzufügen</a>
            </div>
            <div id="new-marker-coordinates"></div>

            <h2>Markierungen</h2>

            <div id="markers"></div>

            <script>
                let newMarkerPosition = null

                window.addEventListener('load', () => {
                    try {
                        const mapsConfig = new MapsConfig('<?= $apiKey ?>')

                        const addMarker = () => {
                            const name = document.getElementById('new-marker-name').value.trim()
                            const address = document.getElementById('new-marker-position').value.trim()

                            if ( ! name.length) {
                                alert('Bitte geben Sie einen Namen für die neue Markierung an')
                            } else {
                                mapsConfig.addMarker(name, address, newMarkerPosition)
                            }
                        }

                        mapsConfig
                            .onSave(() => document.getElementById('mapform').submit())
                            .showMap(<?= $activeMap ?>, '#map', {
                                showLocator: true,
                                image: '<?= $generalConfig->marker->url ?>'
                            }, <?= json_encode($generalConfig) ?>)
                            .showMarkersTable(<?= $activeMap ?>, '#markers')
                            .showPolygonSettings(<?= $activeMap ?>, '#polygon')
                            .initAutoComplete(document.getElementById('new-marker-position'), position => {
                                newMarkerPosition = position

                                document.getElementById('new-marker-coordinates').innerHTML = `Koordinaten: ${position.lat} / ${position.lng}`
                                document.getElementById('btn-add-marker').classList.remove('disabled')
                            })

                        document.getElementById('btn-add-marker').addEventListener('click', addMarker)
                    } catch (e) {
                        console.log(e)
                        alert('Ein unbekannter Fehler ist aufgetreten: ' + e.message)
                    }
                })
            </script>

            <hr>

            <h2>Polygon</h2>

            <p>
                Bitte verwenden Sie <a href="https://www.keene.edu/campus/maps/tool/" target="_blank">dieses Tool</a> um
                ein Polygon zu erstellen oder zu bearbeiten.
            </p>

            <div id="polygon-settings">
                <textarea id="polygon" class="large-text code" rows="20"></textarea>
                <a class="button button-primary disabled" id="btn-polygon">Änderungen übernehmen</a>
            </div>
        </form>
    <?php endif ?>
</div>
